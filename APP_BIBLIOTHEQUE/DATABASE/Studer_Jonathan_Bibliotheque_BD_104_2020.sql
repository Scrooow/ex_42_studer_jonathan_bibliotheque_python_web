-- Database: Studer_Jonathan_Bibliotheque_BD_104
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF exists studer_jonathan_bibliotheque_bd_104_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS studer_jonathan_bibliotheque_bd_104_2020;

-- Utilisation de cette base de donnée

USE studer_jonathan_bibliotheque_bd_104_2020;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 21 Avril 2020 à 10:58
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `studer_jonathan_bibliotheque_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_auteurs`
--

CREATE TABLE `t_auteurs` (
  `id_auteur` int(11) NOT NULL,
  `Nom` varchar(40) NOT NULL DEFAULT '',
  `Prenom` varchar(40) NOT NULL DEFAULT '',
  `Civilite` char(1) NOT NULL DEFAULT '',
  `Date_Naissance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_auteurs`
--

INSERT INTO `t_auteurs` (`id_auteur`, `Nom`, `Prenom`, `Civilite`, `Date_Naissance`) VALUES
(14, 'Christie', 'Agatha', 'F', '1890-09-15'),
(15, 'Corneille', 'Pierre', 'M', '1606-06-06'),
(22, 'Orwell', 'George ', 'M', '1903-06-25'),
(23, 'Hugo', 'Victor', 'M', '1802-02-26'),
(24, 'Zola', 'Émile ', 'M', '1840-04-02'),
(32, 'Ragnar', 'François', '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_emprunts`
--

CREATE TABLE `t_emprunts` (
  `id_emprunt` int(11) NOT NULL,
  `Date_Emprunt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Date_Retour` date NOT NULL,
  `FK_Livre` int(11) NOT NULL,
  `FK_Etudiant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emprunts`
--

INSERT INTO `t_emprunts` (`id_emprunt`, `Date_Emprunt`, `Date_Retour`, `FK_Livre`, `FK_Etudiant`) VALUES
(1, '2020-03-09 11:50:39', '2020-05-20', 1, 3),
(2, '2020-03-09 11:50:39', '2020-05-20', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_etudiants`
--

CREATE TABLE `t_etudiants` (
  `id_etudiant` int(11) NOT NULL,
  `Nom` varchar(40) NOT NULL,
  `Prenom` varchar(40) NOT NULL,
  `Civilite` char(1) NOT NULL,
  `Date_Naissance` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_etudiants`
--

INSERT INTO `t_etudiants` (`id_etudiant`, `Nom`, `Prenom`, `Civilite`, `Date_Naissance`) VALUES
(3, 'Cloro', 'Jonas', 'M', '1997-02-10'),
(4, 'Clero', 'Esteban', 'M', '1980-02-02'),
(5, 'Guidi', 'Roméo', 'M', '1999-02-11'),
(6, 'Gada', 'Espoir', 'M', '2005-08-06'),
(7, 'Liridon', 'Noah', 'M', '1970-02-09');

-- --------------------------------------------------------

--
-- Structure de la table `t_genres_litteraires`
--

CREATE TABLE `t_genres_litteraires` (
  `id_genre_litteraire` int(11) NOT NULL,
  `Genre_Litteraire` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_genres_litteraires`
--

INSERT INTO `t_genres_litteraires` (`id_genre_litteraire`, `Genre_Litteraire`) VALUES
(1, 'Tragi-comédie'),
(2, 'Roman policier'),
(3, 'pasEncoreDefini');

-- --------------------------------------------------------

--
-- Structure de la table `t_livres`
--

CREATE TABLE `t_livres` (
  `id_livre` int(11) NOT NULL,
  `Titre` varchar(100) DEFAULT NULL,
  `Resume` text,
  `Prix` decimal(10,2) DEFAULT NULL,
  `Nb_Pages` int(11) DEFAULT NULL,
  `Date_Parution` date DEFAULT NULL,
  `Numero_ISBN` bigint(11) DEFAULT NULL,
  `FK_Genre_Litteraire` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_livres`
--

INSERT INTO `t_livres` (`id_livre`, `Titre`, `Resume`, `Prix`, `Nb_Pages`, `Date_Parution`, `Numero_ISBN`, `FK_Genre_Litteraire`) VALUES
(1, 'Le Cid', 'Don Diègue et le comte de Gomès projettent d’unir leurs enfants Rodrigue et Chimène, qui s\'aiment. Mais le comte, jaloux de se voir préférer le vieux Don Diègue pour le poste de précepteur du prince, offense ce dernier en lui donnant une gifle (un « soufflet » dans le langage de l\'époque). Don Diègue, trop vieux pour se venger par lui-même, remet sa vengeance entre les mains de son fils Rodrigue qui, déchiré entre son amour et son devoir, finit par écouter la voix du sang et tue le père de Chimène en duel. Chimène essaie de renier son amour et le cache au roi, à qui elle demande la tête de Rodrigue. Mais l’attaque du royaume par les Maures donne à Rodrigue l’occasion de prouver sa valeur et d’obtenir le pardon du roi. Plus que jamais amoureuse de Rodrigue devenu un héros national, Chimène reste sur sa position et obtient du roi un duel entre don Sanche, qui l\'aime aussi, et Rodrigue. Elle promet d’épouser le vainqueur. Rodrigue victorieux reçoit du roi la main de Chimène : le mariage sera célébré l\'année suivante.', '5.50', 188, '1637-03-24', 9788026822714, 1),
(2, 'Dix petits nègres', 'Il se passe quelque chose d\'anormal. Les dix personnes conviées sur l\'ïle du Nègre en ont la certitude. Pourquoi leur hôte est-il absent? Soudain, une voix s\'élève, accusant d\'un crime chaque invité. Commence alors une ronde mortelle, rythmée par les couplets d\'une étrange comptine...\r\n\r\n', '8.55', 320, '2014-08-01', 9782702418116, 2),
(3, 'Les misérables', 'L\'action se déroule en France au cours du premier tiers du xixe siècle, entre la bataille de Waterloo (1815) et les émeutes de juin 1832. On y suit, sur cinq tomes, la vie de Jean Valjean, de sa sortie du bagne jusqu\'à sa mort. Autour de lui gravitent les personnages, dont certains vont donner leur nom aux différents tomes du roman, témoins de la misère de ce siècle, misérables eux-mêmes ou proches de la misère : Fantine, Cosette, Marius, mais aussi les Thénardier (dont Éponine, Azelma et Gavroche) ainsi que le représentant de la loi, Javert.', '8.37', 2598, '1862-01-01', 9781626865037, 3),
(5, 'Apprendre à programmer avec Python 4', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Test_Delete', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_livres_auteurs`
--

CREATE TABLE `t_livres_auteurs` (
  `id_livre_auteur` int(11) NOT NULL,
  `FK_Livre` int(11) NOT NULL,
  `FK_Auteur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_livres_auteurs`
--

INSERT INTO `t_livres_auteurs` (`id_livre_auteur`, `FK_Livre`, `FK_Auteur`) VALUES
(1, 1, 15),
(2, 2, 14);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_auteurs`
--
ALTER TABLE `t_auteurs`
  ADD PRIMARY KEY (`id_auteur`);

--
-- Index pour la table `t_emprunts`
--
ALTER TABLE `t_emprunts`
  ADD PRIMARY KEY (`id_emprunt`),
  ADD KEY `FK_Livre` (`FK_Livre`),
  ADD KEY `FK_Etudiant` (`FK_Etudiant`);

--
-- Index pour la table `t_etudiants`
--
ALTER TABLE `t_etudiants`
  ADD PRIMARY KEY (`id_etudiant`);

--
-- Index pour la table `t_genres_litteraires`
--
ALTER TABLE `t_genres_litteraires`
  ADD PRIMARY KEY (`id_genre_litteraire`);

--
-- Index pour la table `t_livres`
--
ALTER TABLE `t_livres`
  ADD PRIMARY KEY (`id_livre`),
  ADD KEY `FK_Genre_Litteraire` (`FK_Genre_Litteraire`),
  ADD KEY `FK_Genre_Litteraire_2` (`FK_Genre_Litteraire`);

--
-- Index pour la table `t_livres_auteurs`
--
ALTER TABLE `t_livres_auteurs`
  ADD PRIMARY KEY (`id_livre_auteur`),
  ADD KEY `FK_Livre1` (`FK_Livre`),
  ADD KEY `FK_Auteur1` (`FK_Auteur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_auteurs`
--
ALTER TABLE `t_auteurs`
  MODIFY `id_auteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `t_emprunts`
--
ALTER TABLE `t_emprunts`
  MODIFY `id_emprunt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_etudiants`
--
ALTER TABLE `t_etudiants`
  MODIFY `id_etudiant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_genres_litteraires`
--
ALTER TABLE `t_genres_litteraires`
  MODIFY `id_genre_litteraire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_livres`
--
ALTER TABLE `t_livres`
  MODIFY `id_livre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_livres_auteurs`
--
ALTER TABLE `t_livres_auteurs`
  MODIFY `id_livre_auteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_emprunts`
--
ALTER TABLE `t_emprunts`
  ADD CONSTRAINT `t_emprunts_ibfk_1` FOREIGN KEY (`FK_Livre`) REFERENCES `t_livres` (`id_livre`),
  ADD CONSTRAINT `t_emprunts_ibfk_2` FOREIGN KEY (`FK_Etudiant`) REFERENCES `t_etudiants` (`id_etudiant`);

--
-- Contraintes pour la table `t_livres`
--
ALTER TABLE `t_livres`
  ADD CONSTRAINT `t_livres_ibfk_1` FOREIGN KEY (`FK_Genre_Litteraire`) REFERENCES `t_genres_litteraires` (`id_genre_litteraire`);

--
-- Contraintes pour la table `t_livres_auteurs`
--
ALTER TABLE `t_livres_auteurs`
  ADD CONSTRAINT `t_livres_auteurs_ibfk_1` FOREIGN KEY (`FK_Livre`) REFERENCES `t_livres` (`id_livre`),
  ADD CONSTRAINT `t_livres_auteurs_ibfk_2` FOREIGN KEY (`FK_Auteur`) REFERENCES `t_auteurs` (`id_auteur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
