# routes_gestion_livres.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les livres.

from flask import render_template, flash, redirect, url_for, request
from APP_BIBLIOTHEQUE import obj_mon_application
from APP_BIBLIOTHEQUE.LIVRES.data_gestion_livres import GestionLivres
from APP_BIBLIOTHEQUE.DATABASE.erreurs import *
# OM 2020.04.10 Pour utiliser les expressions régulières REGEX
import re


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /livres_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/livres_afficher
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/livres_afficher", methods=['GET', 'POST'])
def livres_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_livres = GestionLivres()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionLivres()
            # Fichier data_gestion_livres.py
            data_livres = obj_actions_livres.livres_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data livres", data_livres, "type ", type(data_livres))

            # OM 2020.04.09 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données livres affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("livres/livres_afficher.html", data=data_livres)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /livres_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "livres_add.html"
# Pour la tester http://127.0.0.1:1234/livres_add
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/livres_add", methods=['GET', 'POST'])
def livres_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_livres = GestionLivres()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "livres_add.html"
            name_livre = request.form['name_livre_html']

            # OM 2019.04.04 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$",
                                name_livre):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")
                # On doit afficher à nouveau le formulaire "livres_add.html" à cause des erreurs de "claviotage"
                return render_template("livres/livres_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_Titre": name_livre}
                obj_actions_livres.add_livre_data(valeurs_insertion_dictionnaire)

                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'livres_afficher', car l'utilisateur
                # doit voir le nouveau livre qu'il vient d'insérer.
                return redirect(url_for('livres_afficher'))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("livres/livres_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /livres_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un livre de films par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/livres_edit', methods=['POST', 'GET'])
def livres_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "livres_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_livre" du formulaire html "livres_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_livre"
            # grâce à la variable "id_livre_edit_html"
            # <a href="{{ url_for('livres_edit', id_livre_edit_html=row.id_livre) }}">Edit</a>
            id_livre_edit = request.values['id_livre_edit_html']

            # Pour afficher dans la console la valeur de "id_livre_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_livre_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_livre": id_livre_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_livres = GestionLivres()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_livre = obj_actions_livres.edit_livre_data(valeur_select_dictionnaire)
            print("dataIdLivre ", data_id_livre, "type ", type(data_id_livre))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le livre d'un film !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("livres/livres_edit.html", data=data_id_livre)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /livres_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un livre de films par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/livres_update', methods=['POST', 'GET'])
def livres_update():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "livres_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du livre alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            # Récupérer la valeur de "id_livre" du formulaire html "livres_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_livre"
            # grâce à la variable "id_livre_edit_html"
            # <a href="{{ url_for('livres_edit', id_livre_edit_html=row.id_livre) }}">Edit</a>
            id_livre_edit = request.values['id_livre_edit_html']

            # Récupère le contenu du champ "intitule_livre" dans le formulaire HTML "LivresEdit.html"
            name_livre = request.values['name_edit_intitule_livre_html']
            valeur_edit_list = [{'id_livre': id_livre_edit, 'intitule_livre': name_livre}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$", name_livre):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "intitule_livre" dans le formulaire HTML "LivresEdit.html"
                #name_livre = request.values['name_edit_intitule_livre_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")

                # On doit afficher à nouveau le formulaire "livres_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "livres_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_livre': 13, 'intitule_livre': 'philosophique'}]
                valeur_edit_list = [{'id_livre': id_livre_edit, 'intitule_livre': name_livre}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "livres_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('livres/livres_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_livre": id_livre_edit, "value_name_livre": name_livre}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_livres = GestionLivres()

                # La commande MySql est envoyée à la BD
                data_id_livre = obj_actions_livres.update_livre_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdLivre ", data_id_livre, "type ", type(data_id_livre))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le livre d'un film !!!")
                # On affiche les livres
                return redirect(url_for('livres_afficher'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème livres update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_livre_html" alors on renvoie le formulaire "EDIT"
            return render_template('livres/livres_edit.html', data=valeur_edit_list)

    return render_template("livres/livres_update.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /livres_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un livre de films par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/livres_select_delete', methods=['POST', 'GET'])
def livres_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_livres = GestionLivres()
            # OM 2019.04.04 Récupérer la valeur de "idLivreDeleteHTML" du formulaire html "LivresDelete.html"
            id_livre_delete = request.args.get('id_livre_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_livre": id_livre_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_livre = obj_actions_livres.delete_select_livre_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur livres_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur livres_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('livres/livres_delete.html', data = data_id_livre)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /livresUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un livre, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/livres_delete', methods=['POST', 'GET'])
def livres_delete():

    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_livres = GestionLivres()
            # OM 2019.04.02 Récupérer la valeur de "id_livre" du formulaire html "LivresAfficher.html"
            id_livre_delete = request.form['id_livre_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_livre": id_livre_delete}

            data_livres = obj_actions_livres.delete_livre_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des livres des films
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les livres
            return redirect(url_for('livres_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "livre" de films qui est associé dans "t_livres_films".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des films !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce livre est associé à des films dans la t_livres_films !!! : {erreur}")
                # Afficher la liste des livres des films
                return redirect(url_for('livres_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur livres_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur livres_delete {erreur.args[0], erreur.args[1]}")


            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('livres/livres_afficher.html', data=data_livres)